# Top Gear Subtitles



## About

This is a work-in-progress project to clean and correct the various iterations of subtitles for the BBC's Top Gear from Series 1 through Series 22. 

This is a long, manual process, so many episodes and specials are currently missing. 

## Changes

- [ ] Removed SDH captions
- [ ] Converted format to UTF-8 SRT.
- [ ] Fixed OCR errors when converting VOBSUB to SRT. Example: "Jop Gear", "7fhree", and "75Q miles."
- [ ] Italicized voiceovers.

